#include <stdio.h>
#include <stdlib.h>
#include "bignum.h"
#include "list.h"

unsigned long int gcd(unsigned long int a, unsigned long int b){
  unsigned long int old_r, r = 1;
  if(a < b)
    return gcd(b,a);

  while(r != 0){
    old_r = r;
    r = a % b;
    a = b;
    b = r;
  }

  return old_r;
}

unsigned long int f(unsigned long int x){
    return x*x+1l;
}

unsigned long int rho_pollard(unsigned long int n){
    list_t l = list_empty();
    list_t head = list_empty();
    unsigned long int xk, i = 2l;
    unsigned long int* j;

    j = malloc(sizeof(unsigned long int));
    *j = i;
    l = list_push(l,j);

    for(;;){
        i = f(i)%n;
        head = l;
        while(!list_is_empty(l)){
            xk = *((unsigned long int*)(l->val));
            if(i == xk){
                l = l->next;
            }
            if(gcd(xk-i,n) != 1)
                return gcd(xk-i,n);
            else
                l = l->next;
        }
        j = malloc(sizeof(unsigned long int));
        *j = i;
        l = list_push(head,j);
    }
}

unsigned long int floyd(unsigned long int n);


int main() {
  // En utilisant l'algorithme rho de Pollard, factorisez les entiers suivants

  unsigned long int n1 = 17 * 113;
  unsigned long int n2 = 239 * 431;
  unsigned long int n3 = 3469 * 4363;
  unsigned long int n4 = 15241 * 18119;
  unsigned long int n5 = 366127l * 416797l;
  unsigned long int n6 = 15651941l * 15485863l;

  bignum_t n7, n8;

  n7 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(127)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(61)),
                             bignum_from_int(1)));

  n8 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(607)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(2203)),
                             bignum_from_int(1)));



  //printf("PGCD(42,24) = %lu\n", gcd(42,24));
  //printf("PGCD(42,24) = %s\n", bignum_to_str(bignum_gcd(bignum_from_int(42),bignum_from_int(24))));

  printf("Rho Pollard 1 = %lu\n", rho_pollard(n1));
  printf("Rho Pollard 2 = %lu\n", rho_pollard(n2));
  printf("Rho Pollard 3 = %lu\n", rho_pollard(n3));
  printf("Rho Pollard 4 = %lu\n", rho_pollard(n4));
  printf("Rho Pollard 5 = %lu\n", rho_pollard(n5));
  printf("Rho Pollard 6 = %lu\n", rho_pollard(n6));




  return 0;
}
