#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <openssl/md5.h>
#include "list.h"

typedef struct pair_t {
  char x[16];
  long int h;
}* pair_t;

int eq_pair(void* a, void* b){
  return ((pair_t)a)->h == ((pair_t)b)->h;
}

/* char x[16] */
long int prod(char* x,int taille){
    long int res = 1;
    for(int i = 0; i< taille-1; i++){
        res = res*x[i];
    }
    return res;
}


int main(){
  pair_t p, q;
  list_t l;
  unsigned int i = 0;

  srand(time(NULL));

  l = list_empty();

  for(;;){
    p = (pair_t)malloc(sizeof(struct pair_t));
    for(int i = 0; i < 15; i++)
      p->x[i] = rand() % (126-32-1) + 32;
    p->x[15] = 0;
    p->h = prod(p->x,16);

    if((q = (pair_t)list_in(l, p, eq_pair)) != NULL){
      printf("After %d iterations, a collision happened !\n", i);
      printf("%s and %s have the same produit.\n", p->x, q->x);

      return 0;
    } else {
      i++;
      l = list_push(l, p);
    }
  }
}
