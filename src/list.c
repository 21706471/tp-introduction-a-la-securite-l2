#include <stdlib.h>

struct cell_t {
  void* val;
  unsigned long int id;
  struct cell_t* next;
};

typedef struct cell_t* list_t;

list_t list_empty(){
  return NULL;
}

int list_is_empty(list_t l){
  return l == NULL;
}

list_t list_push(list_t l, void* x){
  list_t res = malloc(sizeof(struct cell_t));
  res->next = l;
  res->val = x;
  res->id = list_is_empty(l) ? 1 : 1 + l->id;
  return res;
}

list_t list_tail(list_t l){
  return list_is_empty(l) ? NULL : l->next;
}

void* list_pop(list_t* l){
  if(list_is_empty(*l))
    return NULL;
  list_t new_head = (*l)->next;
  void* res = (*l)->val;
  free(*l);
  *l = new_head;
  return res;
}

void* list_top(list_t l){
  return list_is_empty(l) ? NULL : l->val;
}

void list_destroy(list_t l, void (*free)(void*)){
  while(!list_is_empty(l))
    free(list_pop(&l));
}

// return the found element or NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
  while(!list_is_empty(l))
    if(eq(x, l->val))
      return l->val;
    else
      l = l->next;
  return NULL;
}

unsigned long int list_len(list_t l){
  return list_is_empty(l) ? 0 : l->id;
}
